import { Request,  Response} from 'express';
import ContactModel, { Contact } from '../models/Contacts';
import { ResponseModel } from '../models/ResponseModel';
import { Diary } from '../models/Phone';

class ContactController {
    public async GetAllContacts(req: Request, res: Response) : Promise<Response>  {
        const contacts: Contact[] = await ContactModel.find({}, '-gender -diary');
        return res.status(200).json(contacts);
    }

    public async GetContacts(req: Request, res: Response) : Promise<Response>  {
        const contact: Contact | null = await ContactModel.findById(req.params.id);
        return res.status(200).json(contact);
    }

    public async CreateContact(req: Request, res: Response) : Promise<Response>  {
        try {
            const body: Contact = req.body as Contact;
            const newContact: Contact = new ContactModel(body);
            await newContact.save();

            const response: {_id: string, firstName: string, lastName: string} = newContact;

            return res.status(200).json(response);
        } catch (error) {
            console.error(error);
            const response: ResponseModel<string> = {
                Message: '',
                Response: false,
                Result: ''
            }
            response.Response = false;
            response.Message = `Ocurrio un error: ${JSON.stringify(error)}`;
            return res.status(500).json(response);   
        }
    }

    public async UpdateContact(req: Request, res: Response) : Promise<Response>  {
        try {
            let contact: Contact | null = await ContactModel.findByIdAndUpdate(req.params.id, {...req.body});    
            return res.status(200).json(contact);
        } catch (error) {
            console.error(error);
            return res.status(500).json(error);
        }
        
        
    }

    public async DeleteContact(req: Request, res: Response) : Promise<Response>  {
        const response: ResponseModel<string> = {
            Message: '',
            Response: true,
            Result: ''
        }
        try {
            let contact: Contact | null = await ContactModel.findByIdAndDelete(req.params.id);    
            response.Result = `Se actualizo correctamente el contacto ${contact?.firstName}`
        } catch (error) {
            console.error(error);
            response.Response = false;
            response.Message = `Ocurrio un error: ${JSON.stringify(error)}`;
            return res.status(500).json(response);
        }
        
        return res.status(200).json(response);
    }


}


export const contactsController = new ContactController();