import { Router } from 'express';
import { contactsController } from '../controllers/contacts.controllers';


const router: Router = Router();

// Creacion de rutas
router.get('/', contactsController.GetAllContacts)
router.get('/:id', contactsController.GetContacts)
router.post('/', contactsController.CreateContact)
router.put('/:id', contactsController.UpdateContact)
router.delete('/:id', contactsController.DeleteContact)


export default router;