export function capitalizeFLetter(t: string) { 
    return t[0].toUpperCase() + t.slice(1); 
  } 