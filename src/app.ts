import express, {Application} from 'express';
import morgan from 'morgan';
import cors from 'cors';
import './database'

//routes
import ContactRoutes from './routes/contact.routes';

export class App {

    private app: Application;
    private debug: boolean = true;

    constructor(private port?: number | string){
        this.app = express();
        this.settings();
        this.middlewares();
        this.routes();
        
    }

    // CONFIGURACION DE PUERTOS
    settings(){
        this.app.set(
            'port', this.port || process.env.PORT || 3000
        )
    }

    // CONFIGURACION THIRDS PARTY
    middlewares(){
        this.app.use(cors());
        this.app.use(express.json());
        this.app.use(express.urlencoded({extended: true}));

        // encriptacion trafico
        if(this.debug) {
            this.app.use(morgan('dev'));
        } else {
            this.app.use(morgan('short'));
        }
    }

    // RUTAS PADRES
    routes(){
        this.app.use('/contact', ContactRoutes);
    }

    async listen(){
        await this.app.listen(this.app.get('port'));
        console.log(`Server on url http://localhost:${this.app.get('port')}` );
    }

}