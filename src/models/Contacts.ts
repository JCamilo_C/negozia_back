import mongoose, { Schema } from 'mongoose';
import { capitalizeFLetter } from '../helpers/string.helper';
import { Diary } from './Phone';

export interface Contact extends mongoose.Document {
    firstName: string;
    lastName: string;
    gender: string;
    diary: Diary[];
    email: string;
}

const DiarySchema = new Schema({
    type : { type: String },
    phone : { type: String }
})

const ContactSchema = new Schema({
    firstName: String,
    lastName: String,
    gender: String,
    diary: [DiarySchema],
    email: String
});

ContactSchema.path('firstName').set( (v: string) => {
    return v.split(' ').map(capitalizeFLetter).join(' ');
});

ContactSchema.path('lastName').set( (v: string) => {
    return v.split(' ').map(capitalizeFLetter).join(' ');
});

export default mongoose.model<Contact>('Contact', ContactSchema);