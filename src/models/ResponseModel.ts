export interface ResponseModel<T> {
    Response: boolean;
    Message: string;
    Result: T;
};