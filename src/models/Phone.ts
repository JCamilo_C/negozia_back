import mongoose from 'mongoose';

export interface Diary extends mongoose.Document {
    type: string;
    phone: string;
}
