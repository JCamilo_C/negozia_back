import mongoose from 'mongoose';
import { mongodb } from './keys';

mongoose.connect(mongodb.URI, {
    useNewUrlParser: true,
    useUnifiedTopology: true
}).then( db => console.log('la base de datos esta conectada'))
.catch(e => console.error(e));
